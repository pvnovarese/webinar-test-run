FROM xmrig/xmrig:latest AS build1

FROM anchore/test_images:vulnerabilities-centos

MAINTAINER Paul Novarese pvn@novarese.net
LABEL maintainer="pvn@novarese.net"
LABEL name="webinar-test"
LABEL org.opencontainers.image.title="webinar-test"
LABEL org.opencontainers.image.source="https://gitlab.com/pvnovarese/webinar-test-run/"
LABEL org.opencontainers.image.description="Some testing for a webinar by Paul Novarese pvn@novarese.net"

COPY sudo-1.8.23-9.el7.x86_64.rpm /

RUN set -ex && \
    adduser -d /xmrig mining && \
    echo "--BEGIN PRIVATE KEY--" > /private_key && \
    echo "aws_access_key_id=01234567890123456789" > /aws_key && \
    yum -y install /sudo-1.8.23-9.el7.x86_64.rpm && \
    yum -y install python3 && \
    python3 -m ensurepip && \
    pip3 install --index-url https://pypi.org/simple --no-cache-dir pytest urllib3 botocore six numpy && \
    yum autoremove -y && \
    yum clean all && \
    rm -rf /var/cache/yum 

ADD https://github.com/kevinboone/solunar_cmdline.git /solunar_cmdline
COPY --from=build1 /xmrig/xmrig /xmrig/xmrig
USER mining
WORKDIR /xmrig
ENTRYPOINT /bin/false
    
